'use strict'
function MyRound10(val) {
    return Math.round(val / 10) * 10;
}
const prices = [7.6, 7.4, 6.8, 6.1, 5.9, 6.4, 6.8, 7.4, 7.8, 8.083, 8.6, 8.083, 9.0, 9.4, 9.0, 8.083]

class Token {
    price;
    quality;

    constructor(price, quality) {
        this.price = price
        this.quality = quality
    }

    get total() {
        return this.price * this.quality
    }
}

class Wallet {
    token;
    fiat;

    constructor(fiat, token) {
        this.fiat = fiat
        this.token = token
    }

    get total() {
        return this.fiat + this.token.total
    }

    buyToken(token) {
        const sum = this.fiat * 0.25
        const tokenQuality = (sum  + token.total) / token.price
        const tokenForBuy = +(tokenQuality - this.token.quality).toFixed(2)
        this.fiat  = this.fiat - sum
        this.token = new Token(token.price, tokenQuality)
        return {tokenForBuy, newFiat: this.fiat}
    }

    sellToken() {
        // Получаем половину всего кошелька
        const half = this.total / 2
        // Количетсво токенов которое должно быть для 50/50 крипто/фиат
        const newTokenQuality = half / this.token.price
        // Получаем количество токенов которое нужно продать
        const tokenQualityForSell = +Math.abs(this.token.quality - newTokenQuality).toFixed(2)
        // Считаем новый баланс фиата
        this.fiat = this.total - (newTokenQuality * this.token.price)
        // Устанавливаем токен с новым количеством
        this.token = new Token(this.token.price, newTokenQuality)
        return {tokenQualityForSell, newFiat: this.fiat}
    }
}

class Balance {
    prevWallet;
    currentWallet;

    get prevWallet() {
        this.prevWallet
    }
    set prevWallet(wallet) {
        this.prevWallet = wallet
    }

    get currentWallet() {
        this.nextWallet
    }
    set currentWallet(wallet) {
        this.nextWallet = wallet
    }
}
const balance = new Balance()
const averagePrice = 8.082
const prevToken = new Token(averagePrice, 37.1)
const prevWallet = new Wallet(398, prevToken)
balance.prevWallet = prevWallet

prices.forEach(price => {
    let nextToken = null
    let nextWallet = null
    if (!balance.currentWallet) {
        nextToken = new Token(price, prevToken.quality)
        nextWallet = new Wallet(prevWallet.fiat, nextToken)
    } else {
        nextToken = new Token(price, balance.currentWallet.token.quality)
        nextWallet = new Wallet( balance.currentWallet.fiat, nextToken)
    }

    if (nextToken.price < averagePrice) {
        console.log(nextWallet.buyToken(nextToken))
    } else {
        console.log(nextWallet.sellToken())
    }
    balance.currentWallet = nextWallet
    console.log(balance.currentWallet.total)
})

// const nextToken = new Token(8.082, currentWallet.token.quality)
// const nextWallet = new Wallet(currentWallet.fiat, nextToken)
// console.log(nextWallet.sellToken())
// console.log(nextWallet.total)


